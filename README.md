# Prueba Técnica Wellnes

## Decisiiones técnicas
* Para este pequeño ejercicio he utilizado Django junto con Django Rest Framework (DRF) puesto que son las tecnologías con las que estoy más failiarizado.
* La caché es con base de datos para simplificar el desarrollo y la configuración.
* Para la autenticación he utilizado el motor interno de DRF.

## Inicialización del proyecto
Lo primero es clonar el proyecto a nuestro equipo local.
Tras esto con un entorno virtual activo ejecutaremos
```
pip install -r requirements.txt
```
en la carpeta raiz de nuestro proyecto.

## Base de datos
En un entorno postgreSQL, crearemos una base de datos llamada **wellness**. En el fichero **app/settings.py**, la configuración de la base de datos es:

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'wellness',
        'USER': 'postgres',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

Tras esto procedemos a migrar la base de datos y crear las tablas:

```
python manage.py migrate
```

Y también crearemos la tabla de la caché:

```
python manage.py createcachetables
```

## Popular la base de datos
Para cargar los datos, he creado un comando:
```
python manage.py import_csv --file "Monitoring report.csv"
```

A continuación, creamos un usuario para realizar las pruebas:
```
python manage.py create_test_user --username testuser --password test
```

## Activar servidor
Ahora activaremos el servidor para poder probar los distintos endpoints
```
python manage.py runserver
```

Ya podemos realizar peticiones a la API.

## Llamadas
Primero procedemos a solicitar un token para el usuario que hemos creado:
**localhost:8000/api-token-auth/** con el body {"username": "testuser", "password": "test"}

Todas las llamadas deben llevar la cabecera **"Authorization: Token <token>"**, de lo contrario la respuesta será 401.

Procedemos a probar las llamadas. Tenemos tres endpoints diferentes:
* **localhost8000/api/registry/energy-totals?start_date=01-08-2019&end_date=31-08-2019**
Devuelve la energía total, energía reactiva total y potencia total dentro del el rango de fechas especificado. Si no especificamos fechas devuelve los resultados a 0.

* **localhost:8000/api/registry/resumes?start_date=01-8-2019  0:00:00&end_date=31-8-2019 00:00:00&param=power**
Devuelve por cada registro horario las sumas de todas las entradas de base de datos con dicho registro. Si no especificamos las fechas de inicio y fin el resultado estará vacío. También debemos indicar qué parámetro es el que deseamos (power, energy...). Los nombres de los parámetros son los mismos que las cabeceras del csv.

* **localhost:8000/api/registry/current-month-resumes?param=power**
Devuelve el mismo resultado que la anterior, pero tomando como fecha de inicio el día uno del mes en el que nos encontramos y como fecha de fin hoy.

## Testear
Para realizar los tests unitarios:
```
python manage.py test
```

La consola mostrará varios warnings relacionados con las fechas (no utilizamos zona horaria), pero el resultado debe ser **OK**.

## Evolutivos
Como mejora más destacable, añadiría un procesado de datos mñas rápido como puede ser el uso de la librería **pyspark** o cualquier otra que trabaje con técnicas de Big Data.
Luego continuaría implementando el resto de métodos CRUD en la API, un sistema de token almacenados en sesión o implementar una caché más liviana (con Redis por ejemplo).
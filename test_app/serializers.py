from rest_framework.serializers import ModelSerializer

from test_app.models import Registry


class RegistrySerializer(ModelSerializer):
    class Meta:
        model = Registry
        fields = ('__all__')

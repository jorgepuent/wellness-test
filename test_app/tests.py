from datetime import datetime

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import force_authenticate, APIRequestFactory, APITestCase

from  .models import Registry


class Test1(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="test", password="test")
        self.token  = Token.objects.create(user=self.user)
        self.api_authentication()


    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token {}".format(self.token))


    def test_energy_totals_unauthenticated(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(reverse("energy-totals"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_energy_totals_authenticated_with_data(self):
        Registry.objects.create(
            date = datetime(2019, 8, 2),
            energy = 1.0,
            reactive_energy = 2.0,
            power = 1.0,
            maximeter = 1.0,
            reactive_power = 1.0,
            voltage = 1.0,
            intensity = 1.0,
            power_factor = 1.0,
        )       
        Registry.objects.create(
            date = datetime(2019, 8,3),
            energy = 1.0,
            reactive_energy = 2.0,
            power = 7.0,
            maximeter = 1.0,
            reactive_power = 1.0,
            voltage = 1.0,
            intensity = 1.0,
            power_factor = 1.0,
        )


        response1 = self.client.get( "{}?start_date=01-8-2019&end_date=31-8-2019".format(reverse("energy-totals")))
        expected_results1 = {"energy_total": 2.0, "reactive_energy_total": 4.0, "power_total": 8.0}
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.data["energy_total"], expected_results1["energy_total"])
        self.assertEqual(response1.data["reactive_energy_total"], expected_results1["reactive_energy_total"])
        self.assertEqual(response1.data["power_total"], expected_results1["power_total"])

        response2 = self.client.get(reverse("energy-totals"))
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.data["energy_total"], 0)
        self.assertEqual(response2.data["reactive_energy_total"], 0)
        self.assertEqual(response2.data["power_total"], 0)

        response3 = self.client.get( "{}?start_date=01-8-2019 00:00:00&end_date=31-8-2019 00:00:00&param=voltage".format(reverse("energy-resumes")))
        expected_results3 = {"02-08-2019 00:00:00": 1.0, "03-08-2019 00:00:00": 1.0}
        self.assertEqual(response3.status_code, status.HTTP_200_OK)
        self.assertEqual(response3.data["02-08-2019 00:00:00"], expected_results3["02-08-2019 00:00:00"])
        self.assertEqual(response3.data["03-08-2019 00:00:00"], expected_results3["03-08-2019 00:00:00"])




from datetime import date, datetime, timedelta
from functools import reduce

from django.core.cache import cache
from django.db.models import Sum

from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from test_app.models import Registry
from test_app.serializers import RegistrySerializer



class RegistryEnergyTotals(APIView):
    permission_classes = (IsAuthenticated,)    

    def get(self, request, format=None):
        if not request.GET.get("start_date", None):
            today = date.today()
            start_date = today.replace(day=1)
        else:
            start_date = datetime.strptime(request.GET["start_date"], "%d-%m-%Y")
        
        if not request.GET.get("end_date", None):
            next_month = today.replace(day=28) + timedelta(days=4)
            end_date = next_month - timedelta(days=next_month.day)
        else:
            end_date = datetime.strptime(request.GET["end_date"], "%d-%m-%Y")

        registries = Registry.objects.filter(date__range=(start_date, end_date))
        serializer = RegistrySerializer(registries, many=True)
        
        if registries:
            if not cache.get("energy_total"):
                filtered_values = filter(lambda data: data["energy"], serializer.data)
                mapped_values = map(lambda data: data["energy"], filtered_values)
                energy_total = reduce(lambda val1, val2: val1 + val2, mapped_values)
                
                cache.set("energy_total", energy_total)
            
            if not cache.get("reactive_energy_total"):
                filtered_values = filter(lambda data: data["reactive_energy"], serializer.data)
                mapped_values = map(lambda data: data["reactive_energy"], filtered_values)
                energy_total = reduce(lambda val1, val2: val1 + val2, mapped_values)
                
                cache.set("reactive_energy_total", energy_total)

            if not cache.get("power_total"):
                filtered_values = filter(lambda data: data["power"], serializer.data)
                mapped_values = map(lambda data: data["power"], filtered_values)
                energy_total = reduce(lambda val1, val2: val1 + val2, mapped_values)
                
                cache.set("power_total", energy_total)

            return Response({
                "energy_total": cache.get("energy_total"),
                "reactive_energy_total": cache.get("reactive_energy_total"),
                "power_total": cache.get("power_total"),
            })

        return Response({
                "energy_total": 0,
                "reactive_energy_total": 0,
                "power_total": 0
        })


class RegistryResumes(APIView):
    permission_classes = (IsAuthenticated,)    

    def get(self, request, format=None):
        if not request.GET.get("param", None):
            return Response("You must specify a param")
        
        start_date, end_date = None, None

        if request.GET.get("start_date", None):
            start_date = datetime.strptime(request.GET["start_date"], "%d-%m-%Y %H:%M:%S")
        
        if request.GET.get("end_date", None):
            end_date = datetime.strptime(request.GET["end_date"], "%d-%m-%Y %H:%M:%S")

        if not start_date or not end_date:
            return Response({})

        param = request.GET["param"]
        if not cache.get(f"{param}_resumes_between_{start_date}-{end_date}"):
            registries = Registry.objects.filter(date__range=(
                start_date, end_date
            )).values("date", param).annotate(Sum(param)).order_by('date')

            data ={}
            for registry in registries:
                data[registry["date"].strftime("%d-%m-%Y %H:%M:%S")] = registry[param]
            
            cache.set(f"{param}_resumes_between_{start_date}-{end_date}", data)

        return Response(cache.get(f"{param}_resumes_between_{start_date}-{end_date}"))



class CurrentMonthRegistryResumes(APIView):
    permission_classes = (IsAuthenticated,)    

    def get(self, request, format=None):
        if not request.GET.get("param", None):
            return Response("You must specify a param")
        
        today = date.today()
        start_date = today.replace(day=1)

        param = request.GET["param"]
        if not cache.get(f"{param}_current_month_resumes"):
            registries = Registry.objects.filter(date__range=(
                start_date, today
            )).values("date", param).annotate(Sum(param)).order_by('date')

            data ={}
            for registry in registries:
                print(registry["date"])
                data[registry["date"].strftime("%d-%m-%Y %H:%M:%S")] = registry[param]
            
            cache.set(f"{param}_current_month_resumes", data)

        return Response(cache.get(f"{param}_current_month_resumes"))

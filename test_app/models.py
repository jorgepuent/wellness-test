from django.db import models


class Registry(models.Model):

    date = models.DateTimeField(auto_now=False, auto_now_add=False)

    energy = models.FloatField(null=True)

    reactive_energy = models.FloatField(null=True)

    power = models.FloatField(null=True)
    
    maximeter = models.FloatField(null=True)

    reactive_power = models.FloatField(null=True)
    
    voltage = models.FloatField(null=True)

    intensity = models.FloatField(null=True)

    power_factor = models.FloatField(null=True)

    class Meta:
        verbose_name = "Registry"
        verbose_name_plural = "Registries"

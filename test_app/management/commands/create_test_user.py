from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from rest_framework.authtoken.models import Token


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--username', type=str)
        parser.add_argument('--password', type=str)

    def handle(self, *args, **options):
        user = User.objects.create(username=options["username"],password=options["password"])
        token = Token.objects.create(user=user)
        return "User created!"

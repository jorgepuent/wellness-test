import csv
from datetime import datetime

from django.core.management.base import BaseCommand

from test_app.models import Registry


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        if not options["file"]:
            return "You must provide the file path"
        
        with open(options["file"]) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            counter = 0
            for row in csv_reader:
                if counter > 0:
                    date = datetime.strptime(row[0], "%d %b %Y %H:%M:%S")
                    Registry.objects.create(
                        date= date,
                        energy= row[1] if row[1] != "" else None,
                        reactive_energy= row[2] if row[1] != "" else None,
                        power= row[3] if row[3] != "" else None,
                        maximeter= row[4] if row[4] != "" else None,
                        reactive_power= row[5] if row[5] != "" else None,
                        voltage= row[6] if row[6] != "" else None,
                        intensity=  row[7] if row[7] != "" else None,
                        power_factor= row[8] if row[8] != "" else None,
                    )

                counter += 1
                
        return f"{counter} files created"
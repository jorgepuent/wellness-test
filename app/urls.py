from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from django.views.generic.base import TemplateView

from rest_framework.authtoken.views import obtain_auth_token

from test_app.views.api import RegistryEnergyTotals, RegistryResumes, CurrentMonthRegistryResumes



urlpatterns = [
    url(r"admin/", admin.site.urls),
    #API
    url(r"^api/registry/energy-totals", RegistryEnergyTotals.as_view(), name="energy-totals"),
    url(r"^api/registry/resumes", RegistryResumes.as_view(), name="energy-resumes"),
    url(r"^api/registry/current-month-resumes", CurrentMonthRegistryResumes.as_view(), name="current-month-resumes"),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]
